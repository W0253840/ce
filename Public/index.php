<?php
session_start();

require_once '../Controller/siteController.php';
require_once '../Controller/userController.php';
require_once '../Controller/fileController.php';

$siteController = new SiteController();
$userController = new UserController();
$fileController = new FileController();

if(isset($_POST['serverLogoutSubmit']))
{
    //logout button pressed

    session_destroy();
    session_start();

    $_POST = array();

    $siteController->displayActionServerLogin();

}
elseif(isset($_POST['serverLoginSubmit']) && !empty($_POST['loginUser']) && !empty($_POST['loginPwd']))
{
    //login form submitted
    $userController->validateServerUser($_POST['loginUser'],$_POST['loginPwd']);

}
elseif(isset($_POST['fileBrowser']))
{
    //login form submitted
    $siteController->displayActionFileServer();

}
elseif(isset($_POST['agentUploadSubmit']))
{
    $siteController->displayActionAgentUploader();
}
elseif(isset($_FILES['files']))
{
       $fileController->saveFiles($_FILES['files']);

}
elseif(isset($_SESSION['openFile']) && isset($_GET['searchTerm'])){
    //search selected file
    $fileController->searchFile(unserialize($_SESSION['openFile']), $_GET['searchTerm']);
}
elseif(isset($_GET['viewFile'])){
    //open selected file
    $siteController->displayActionFileSearch($_GET['viewFile']);
}
elseif(isset($_GET['deleteFile'])){
    //delete selected file
    $fileController->deleteFile($_GET['deleteFile']);
}
elseif(!empty($_SESSION['serverUser']))
{
    //display file browser on login
    $siteController->displayActionFileServer();
}
else
{
    $siteController->displayActionServerLogin();
}
?>
