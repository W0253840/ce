<?php

class User{

    private $user_id;
    private $name;
    private $password;
    private $salt;


    public function __construct($user_id,$name,$password,$salt)
    {
        $this->user_id=$user_id;
        $this->setName($name);
        $this->setPassword($password);
        $this->setSalt($salt);
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getUserId()
    {
        return $this->user_id;
    }



}//end User class
