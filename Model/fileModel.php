<?php

require_once '../model/Data/aDataAccess.php';
require_once 'file.php';

class FileModel{

    private $serverPath = "../Resources/";

       //get all file rows from db and return as array of objects
      public function getAllFiles(){

          $arrayOfFileObjects="";//initialize array of file objects

          $myDataAccess = aDataAccess::getInstance();
          $myDataAccess->connectToDB();

          $myDataAccess->getAllFiles();

          //populate array of file objects
          while($record = $myDataAccess->fetchFiles()){
                $fetchedFile = new File($myDataAccess->fetchFileId($record),
                                        $myDataAccess->fetchFileName($record),
                                        $myDataAccess->fetchFilePath($record));

                //add file object to array
               $arrayOfFileObjects[] = $fetchedFile;
          }//end while

           $myDataAccess->closeDB();

          return $arrayOfFileObjects;

      }//end getAllFiles


    public function getFileById($id){

        $myDataAccess = aDataAccess::getInstance();
        $myDataAccess->connectToDB();

        $myDataAccess->getFileById($id);
        $record = $myDataAccess->fetchFiles();

        $fetchedFile = new File($myDataAccess->fetchFileId($record),
                                    $myDataAccess->fetchFileName($record),
                                    $myDataAccess->fetchFilePath($record));

        $myDataAccess->closeDB();

        $_SESSION['openFile']= serialize($fetchedFile);//store in session to store info for searching

        return $fetchedFile;

    }//end getAllFiles

    public function checkUploadErrors($files){
//TODO:
///http://php.net/manual/en/features.file-upload.php
        //first example and check
                //http://www.php.net/manual/en/features.file-upload.errors.php


    }

    public function saveFiles($files){

        $uploadCount=0;
        $myDataAccess = aDataAccess::getInstance();
        $myDataAccess->connectToDB();

        foreach ($files['name'] as $i => $name) {
            if (strlen($files['name'][$i]) > 1) {
                if (move_uploaded_file($files['tmp_name'][$i], $this->serverPath .$name)) {
                    $uploadCount++;
                    $myDataAccess->addFile($files['name'][$i], $this->serverPath . $name);


                }//end if
            }//end if
        }//end foreach
        $myDataAccess->closeDB();
        return $uploadCount;
    }

    public function searchFile($file, $query){

        $filePath=$file->getPath();



            $matches = array();

            $handle = @fopen($filePath, "r");
            if ($handle)
            {
                while (!feof($handle))
                {
                    $buffer = fgets($handle);
                    if(strpos($buffer, $query) !== FALSE)
                        $matches[] = $buffer;
                }
                fclose($handle);
            }

        //return results:
        return $matches;

    }

    public function deleteFile($fileId){

        $myDataAccess = aDataAccess::getInstance();
        $myDataAccess->connectToDB();
        //delete file record from db
        $myDataAccess->deleteFile($fileId);

        $myDataAccess->closeDB();




    }
}//end FileModel class