<?php

class File{

    private $file_id;
    private $name;
    private $path;

    public function __construct($file_id,$name,$path)
    {
        $this->file_id=$file_id;
        $this->setName($name);
        $this->setPath($path);

    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setPath($path)
    {
        $this->path = $path;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getFileId()
    {
        return $this->file_id;
    }



}//end File class
