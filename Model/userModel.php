<?php

require_once '../model/Data/aDataAccess.php';
require_once 'user.php';

class UserModel{

    //returns true if user's login credentials are correct
    public function validateServerUser($inputName,$inputPassword){

        $isValid=false;
        //check if username in db and get salt
        $loginUser = self::getUserByName($inputName);

        if(!empty($loginUser)){
        //if login user found
            if($this->validatePassword($loginUser, $inputPassword))
            {
                //if salted input password corresponds
                return true;

            }else{
                return false;

            }//endif
        }else{
            return false;
        }//endif
    }//end validateServerUser


    public static function getUserByName($name){
        $myDataAccess = aDataAccess::getInstance();
        $myDataAccess->connectToDB();

        $myDataAccess->selectUserByName($name);

        $record =  $myDataAccess->fetchUsers();

        $fetchedUser = new User($myDataAccess->fetchUserID($record),
            $myDataAccess->fetchUserName($record),
            $myDataAccess->fetchUserPassword($record),
            $myDataAccess->fetchUserSalt($record));

        $myDataAccess->closeDB();

        return $fetchedUser;
    }//end getUserByName

    public function validatePassword($User, $password){

        $loginPwd = stripslashes($password);

        $salt = $User->getSalt();

        if(!is_null($salt)){
            //check salt against login password
            $hashInstructions = '$6$rounds=3000$' . $salt . '$';
            $hashedPasswordTemp = crypt($loginPwd, $hashInstructions);
            $hashedPwd = str_replace($hashInstructions, "", $hashedPasswordTemp);

            //returns 0 if passwords are equal
            $isEqualsInt = strcmp ($User->getPassword() ,$hashedPwd);

            if($isEqualsInt==0){

                return true;

            }else{
                return false;

            }//end if

        }else{
            return false;
        }//end if

    }//end validatePassword


}//end UserModel class