<?php
//adapter class in case of multiple db connections

abstract class aDataAccess
{
    private static $m_DataAccess;

    public static function getInstance()
    {
        // singleton
        if(self::$m_DataAccess == null)
        {
            require_once 'dataAccess.php';
            self::$m_DataAccess = new DataAccess();

        }//end if
        return self::$m_DataAccess;
    }//end getInstance

    public abstract function connectToDB();

    public abstract function closeDB();

    public abstract function selectUserById($id);

    public abstract function selectUserByName($name);

    public abstract function fetchUsers();

    public abstract function fetchUserName($row);

    public abstract function fetchUserId($row);

    public abstract function fetchUserPassword($row);

    public abstract function fetchUserSalt($row);

}//end abstract aDataAccess class

