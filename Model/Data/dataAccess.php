<?php

require_once 'aDataAccess.php';
class DataAccess extends aDataAccess
{
    private $dbConnection;
    private $result;

    // aDataAccess methods
    public function connectToDB()
    {
        //TODO: connect as own user, not root
        $this->dbConnection = @new mysqli("localhost","root", "","textup");
        if (!$this->dbConnection)
        {
            die('Could not connect to database: ' .
                $this->dbConnection->connect_errno);
        }

    }

    public function closeDB()
    {
        $this->dbConnection->close();
    }

    public function getAllFiles(){

        $this->result = @$this->dbConnection->query("SELECT file_id, file_name, file_path FROM file");
        if(!$this->result)
        {
            die('Could not connect to database: ' .
                $this->dbConnection->error);
        }

    }


    public function selectUserById($id){

        $this->result = @$this->dbConnection->query("SELECT * FROM user WHERE user_id =" . $id );
        if(!$this->result)
        {
            die('Could not connect to database: ' .
                $this->dbConnection->error);
        }

    }


    public function selectUserByName($name)
    {
        $preparedStatement =$this->dbConnection->prepare('SELECT * FROM user WHERE user_name =?');
        $preparedStatement->bind_param('s', $name);
        $preparedStatement->execute();
        $this->result = $preparedStatement->get_result();

        if(!$this->result)
        {
            die('Could not connect to database: ' .
                $this->dbConnection->error);
        }


    }//end selectUserByName


    public function fetchUsers(){

        if(!$this->result)
        {
            die('No records in the result set: ' .
                $this->dbConnection->error);
        }
        return $this->result->fetch_array();


    }//end fetchUsers


    public  function fetchUserName($row)
    {
        return $row['user_name'];
    }

    public  function fetchUserId($row)
    {
        return $row['user_id'];
    }

    public  function fetchUserPassword($row)
    {
        return $row['user_password'];
    }

    public  function fetchUserSalt($row)
    {
        return $row['user_salt'];
    }

    public  function fetchFiles()
    {
        if(!$this->result)
        {
            die('No records in the result set: ' .
                $this->dbConnection->error);
        }
        return $this->result->fetch_array();
    }

    public function getFileById($id)
    {
        //prevent sql injection
        $preparedStatement =$this->dbConnection->prepare('SELECT file_id, file_name, file_path FROM file WHERE file_id =?');
        $preparedStatement->bind_param('i', $id);
        $preparedStatement->execute();
        $this->result = $preparedStatement->get_result();

        if(!$this->result)
        {
            die('Could not retrieve records from database: ' .
                $this->dbConnection->error);
        }
    }

    public function addFile($name, $path)
    {
        //prevent sql injection
        $preparedStatement =$this->dbConnection->prepare('INSERT INTO file(file_name, file_path) VALUES (?,?)');
        $preparedStatement->bind_param('ss', $name, $path);
        $this->result = $preparedStatement->execute();

        if(!$this->result)
        {
            die('Could not retrieve records from database: ' .
                $this->dbConnection->error);
        }
    }

    public function deleteFile($id)
    {
        //prevent sql injection
        $preparedStatement =$this->dbConnection->prepare('DELETE FROM file WHERE file_id = ?');
        $preparedStatement->bind_param('i', $id);
        $this->result = $preparedStatement->execute();

        if(!$this->result)
        {
            die('Could not delete records from database: ' .
                $this->dbConnection->error);
        }

    }

    public function fetchFileId($row)
    {
        return $row['file_id'];

    }

    public function fetchFilePath($row)
    {
        return $row['file_path'];

    }

    public function fetchFileName($row)
    {
        return $row['file_name'];

    }
}//end aDataAccess class
