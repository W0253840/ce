<?php
/**
 * Created by JetBrains PhpStorm.
 * User: inet2005
 * Date: 2/21/14
 * Time: 11:17 AM
 * To change this template use File | Settings | File Templates.
 */
require_once('fileController.php');


class SiteController
{

    public function displayActionServerLogin()
    {
        include '../view/serverLogin.php';
    }

    public function displayActionAgentUploader(){


        include '../view/Agent/fileUpload.php';
    }

    public function displayActionFileServer(){

        $fileController = new FileController();
        $fileController->listFiles();

    }

    public function displayActionFileSearch($id){

    $fileController = new FileController();
    $fileController->getFile($id);

    }



}//end SiteController class