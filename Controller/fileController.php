<?php

require_once('../model/fileModel.php');
require_once('siteController.php');

class FileController
{
    private $model;

    public function __construct()
    {
        $this->model = new FileModel();

    }

    public function listFiles()
    {

        $arrayOfFileObjects = $this->model->getAllFiles();
        include '../view/fileBrowse.php';
    }

    public function getFile($fileId)
    {
        $selectedFile = $this->model->getFileById($fileId);
        include '../view/fileSearch.php';
    }

    public function deleteFile($fileId){

        $this->model->deleteFile($fileId);

        $this->listFiles();
    }

    public function saveFiles($files)
    {

        $uploadCount = $this->model->saveFiles($files);

        $siteController = new SiteController();
        echo "<h3>" . $uploadCount . " files uploaded successfully.</h3>";
        $siteController->displayActionAgentUploader();
    }

    public function searchFile($file, $query)
    {

        $results = $this->model->searchFile($file, $query);

        $selectedFile = $file;
        include '../view/fileSearch.php';

    }



}//end FileController class