<?php

require_once('../model/userModel.php');
require_once('siteController.php');

class UserController
{
    private $model;
    private $site;
    public function __construct()
    {
        $this->model = new userModel();
        $this->site = new siteController();
    }

    public function validateServerUser($inputName,$inputPassword){


        if($this->model->validateServerUser($inputName,$inputPassword)){

            $_SESSION['serverUser']= $inputName;//TODO: set current user in model
            $this->site->displayActionFileServer();

        }
        else{
            //TODO: make sure this displays properly
            $this->site->displayActionServerLogin();//TODO:this might create an unnecessary page reload
            echo "<h2>Invalid Login Information</h2>";

        }
    }

}//end UserController class