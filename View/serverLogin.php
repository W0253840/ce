<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Textup: Log in</title>

        <!-- Bootstrap -->
        <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="/bootstrap/css/bootstrap-theme.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <div class="wrapper">
            <table class="table table-striped table-bordered table-condensed">
                <tr>
                    <td colspan="3"><strong>Login to TextUp Server</strong></td>
                </tr>

                <form name="userMenu" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" id="agentForm">
                    <input id='agent' name='agentUploadSubmit' type='submit' value='agentUpload' class='btn-primary'>
                </form>

                <tr>
                <form name="serverLogin" method="post">
                <td>
                    <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#FFFFFF">

                        <tr>
                            <td width="78">Username</td>
                            <td width="6">:</td>
                            <td width="294"><input name="loginUser" type="text" id="loginUser"></td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td>:</td>
                            <td><input name="loginPwd" type="password" id="loginPwd"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><input type="submit" name="serverLoginSubmit" value="Login" class='btn-'></td>
                        </tr>
                    </table>
                </td>
            </form>
            </tr>
            </table>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../Public/bootstrap/js/bootstrap.min.js"></script>

        <!-- Bootstrap -->
        <link href="../Public/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    </body>
</html>
