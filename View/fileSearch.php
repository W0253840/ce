<?php

if(empty($_SESSION['serverUser'])){
    require_once '../controller/siteController.php';
    $siteController = new SiteController();
    echo "<h3>You must be logged in to view this page.</h3>";
    $siteController->displayActionServerLogin();
}
else{
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Textup: Search Files</title>

    <!-- Bootstrap -->
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/bootstrap/css/bootstrap-theme.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div class="wrapper">
<strong>Server Menu:</strong>
<form name="userMenu" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" id="logout">
    <input id='fileBrowser' name='fileBrowserSubmit' type='submit' value='Browse Files'>
    <input id='serverLogout' name='serverLogoutSubmit' type='submit' value='Logout'>
</form>

<form name="searchForm" method="get" id="searchForm">
    <td>
        <table width="25%" border="0" cellpadding="3" cellspacing="1" bgcolor="#FFFFFF">

            <tr>
                <td>Search</td>
                <td>:</td>
                <td><input name="searchTerm" type="text" id="searchTerm"></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><input name="searchTermSubmit" type="submit" value="Search"></td>
            </tr>
        </table>
    </td>
</form>

<ul class="list-group list-group-wrapper">
    <?php

    if(!empty($results)){

        echo count($results) . " occurrences found.";

        $count=0;

        foreach($results as $result){

            $count++;

            echo "<li class='list-group-item'>";
            echo $count . ". " . $result;
            echo "</li>";

        }

    }
    ?>

</ul>
<pre>
<?php

$filePath = $selectedFile->getPath();


if (file_exists($filePath)) {
    //  header('Content-Description: File Transfer');
    //  header('Content-Type: application/octet-stream');
    //  header('Content-Disposition: attachment; filename='.basename($file));
    //  header('Expires: 0');
    //  header('Cache-Control: must-revalidate');
    // header('Pragma: public');
    //header('Content-Length: ' . filesize($file));
    // ob_clean();
    //  flush();
    ob_start();
    readfile($filePath);
    $data = ob_get_clean();
    echo htmlentities($data);
    exit;
}//end if

}//end if
?>
</pre>
</div>
</body>
</html>